# Traits

Traits are intrinsic abilities of a given type.

Any type can implement any trait, as long as it have a logical meaning.

Each type comes with its own list of implemented traits, and the respective type documentation should be reffered to know these ones.

The full list of existing traits in Mélodium is presented in next section, as well as the core types traits implementations.

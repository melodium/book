# Runtime

Mélodium uses a runtime engine. The script files are fully parsed and their logic build and checked _before_ any execution starts. When launching a Mélodium script, multiple stages happens:

1. Script textual parsing and semantic build
1. Usage and depedencies resolution
1. Logic building
1. Models instanciation
1. Execution and tracks triggering

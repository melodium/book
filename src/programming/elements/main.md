# Elements

This chapter explains the usage and implementation of Mélodium elements, that are:
- treatments,
- models,
- contexts,
- functions,
- data types.
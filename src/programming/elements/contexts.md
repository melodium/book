# Contexts

Contexts are data available through a whole track.
Unlike parameters, they inherently exists and are accessible by all treatements requiring them, as long as the source of the track they are in provide it.

The calling treatments don't have to explicitly pass any context to their inner called treatements.
Contexts have special naming convention, starting with `@`.

## Context providing and requirement

Sources provide contexts, and treatments connected afterwards this source can require context to access it.

> Requiring a context means the treatment can _only_ be used in a track coming from a source providing such context.

```mel
use http/server::HttpServer
use http/server::connection
use http/method::|get as |methodGet
use http/server::@HttpRequest
use std/data::|get

treatment myApp[http_server: HttpServer]() {
    connection(method=|methodGet(), route="/user/:user")
    actionGetUser()

    connection.data -> actionGetUser.data,result -> connection.data
}

treatment actionGetUser()
  require @HttpRequest
  input  data:   Stream<byte>
  output result: Stream<byte>
{
    getUser(user_id = |get<string>(@HttpRequest[parameters], "user"))

    Self.data -> getUser.data,result -> Self.result
}

treatment getUser(user_id: Option<string>)
  input data   :  Stream<byte>
  output result: Stream<byte>
{
    // Do some stuff…
}
```
_Reference for [HttpServer](https://doc.melodium.tech/0.8.0-rc3/en/http/server/HttpServer.html), [connection](https://doc.melodium.tech/0.8.0-rc3/en/http/server/connection.html), [|get (http method)](https://doc.melodium.tech/0.8.0-rc3/en/http/method/|get.html), [@HttpRequest](https://doc.melodium.tech/0.8.0-rc3/en/http/server/@HttpRequest.html),[|get (map access)](https://doc.melodium.tech/0.8.0-rc3/en/std/data/|get.html)_

![myApp graph](images/myApp_context.svg)  
_myApp_

![actionGetUser graph](images/actionGetUser_context.svg)  
_actionGetUser_

As contexts comes at track creation, they are inherently _variable_ data.

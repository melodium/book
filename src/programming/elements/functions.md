# Functions

Functions are directly callable elements, as in any other programming language, they take parameters and return a value.

Functions in Mélodium are always pure, meaning they don't have any side effects.

Functions are executed at program initialisation or track creation, depending if their return value is used as _constant_ or _variable_ parameter.

Functions are recognizable by the `|` symbol starting their name.

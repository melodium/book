# Data types

Data types are very basically the definition of data that can be given as parameter, transmitted through inputs and outputs, and more generally used across functions.

Mélodium packages can define their own data types, completing the _core types_, and they can be `use`-d in any place data type can be given.

One of the most common data type is the standard [`Map`](https://doc.melodium.tech/0.8.0-rc3/en/std/data/Map.html).

Data types can implements _traits_ and be used as _generics_ in some places.
# Parameters

Treatments and models declares parameters. Parameters are like in any language: elements given by the caller to set up behavior of the model or treatment.

# Const and var

In Mélodium, parameters can be either constant or variable, respectively declared with keywords `const` and `var`.
A _constant_ parameter designates something that will keep the same value during all the execution, on all tracks generated through the given call. They are used mostly to configure models, that have all parameters required to be constant.
A _variable_ parameter designates something that may have different values on each track generated.

While a constant can be used to set up constant and variable parameters, variable elements (parameters but also contextes) can only be used to set up other variables.

# Reference

The Mélodium reference is available on [doc.melodium.tech](https://doc.melodium.tech/latest/en/).
The whole Standard Library is documented there, and can be browsed through areas.
# Concepts

Mélodium is a programming language oriented to various data, signal and trigger treatment, process orchestration, and infrastructure management.
Do to so, it relies on some concepts that are explained in this chapter.
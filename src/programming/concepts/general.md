# General Orchestration

Mélodium is a language fully oriented to what happens with data.
As such, in Mélodium, data and signals follows an ensemble of paths that brings it to different kind of treatments and processes.

Unlike many programming languages, Mélodium makes a total abstraction of the instruction order and don't rely on a line-by-line execution scheme.
Every element can run and be shared among different threads depending on the load, data availability, or orchestration optimization decision.

To proceed with those ideas, two major elements exists in Mélodium: _models_ and _treatments_.
Both are essential and can summarize the whole power of Mélodium, and are briefly explained here, more extensive explanations are made in their dedicated [Elements chapters](../elements/main.md).

## Models

Models are elements that live through the execution of a program. They are the elements from where the events occur, data arrive, and can be shared with the outside.
A filesystem agent, a HTTP server, or a SQL connection, can, among many others, be models.

Models can be declared and designed over other models, inheriting their functionnal abilities.

Models are usually instancied by some applicative treatment, and passed as parameter to their inner treatments.
Most models have an immediate startup behavior, such as the [`JavasScriptEngine` model](https://doc.melodium.tech/0.8.0-rc3/en/javascript/JavaScriptEngine.html), while some may have specific startup trigger, like the [`SqlPool` model](https://doc.melodium.tech/0.8.0-rc3/en/sql/SqlPool.html), that wait to have some query to do to try connection if its `min_connections` parameter is set to 0.
Finally, some models are inherently passive, like the [`Environment` model](https://doc.melodium.tech/0.8.0-rc3/en/process/Environment.html) that defines the executive environment used for subsequent processes.

<!-- ```mel
// Example of model being a JavaScriptEngine,
// and having a simple JS function declaration
// as inner code.

model CustomJsEngine(): JavaScriptEngine {
    code = "function greet(someone) {
        return {
            response: `Hello ${someone}!`
        }
    }"
}
``` -->

## Treatments

Treatments describes flows of operations that applies on data. They can be seen as maps, on which paths connects from sources to destinations, browsing through different locations with different purposes.

Treatments can contains other treatments, as a function can call other functions. However, within a treatment the order of declaration has no importance, treatments will run when there are data ready to be processed, and all treatments can be considered as running simultaneously.

Treatments have inputs and outputs, that can be linked with _connections_.
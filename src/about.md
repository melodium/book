# About the author

Quentin Vignaud is IT engineer graduated from [CESI](https://www.cesi.fr/), and M.Sc. in computing science from [UQÀM](https://uqam.ca/).
Working at [Doctolib](https://doctolib.com/) as data and software engineer, originally authored Mélodium during studies at UQÀM, while doing scientific research in music analysis.

Website: <https://www.quentinvignaud.com/>

# Run

To run a packaged Mélodium application, the host system must have Mélodium installed.

Considering the file is `myapp.jeu`, it can be run through direct call or with explicit `melodium` command:
```sh
// Direct call
$ ./myapp.jeu

// Explicit Mélodium call
$ melodium run myapp.jeu
```
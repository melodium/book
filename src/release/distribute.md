# Distribute

`Jeu` files can be distributed as-is. The host machine only need Mélodium to be installed.

> `Jeu` files are already highly compressed data (using the LZMA2 algorithm), storing and distributing them in re-compressed files is not useful.
# Package

To build a packaged Mélodium application, run the `melodium jeu build` command:

```shell
$ melodium jeu build <PROJECT_LOCATION> <OUTPUT_FILE>
```

This will check the given project and package it in a resulting `.jeu` file.
# Installation

Mélodium can be installed through different ways.
There are prebuilt binaries ready to use for major platforms, as well as installers.

## Installers and binaries

For major platforms, such as Linux, Mac, and Windows, Mélodium is released as package or installer, depending on the common practice related to the operating system. Standalone binaries stored in archive files are also available for all these platforms.

> Please refer to the [Download Page](https://melodium.tech/docs/category/download) to get installer and prebuilt binaries.

## Compilation

Mélodium can be build and installed through the Rust `cargo` command.
This method is available for all supported platforms, and require to have the Rust Environment installed and ready to work.
Please refer to the [Rust Project](https://www.rust-lang.org/tools/install) to install the Rust compiler.

To build and install Mélodium:
```sh
cargo install melodium
```

Please note this may take a while, depending on the network connection and machine proceeding to compilation.

Refer to the [Supported Platforms](../software/supported_platforms.md) chapter to check the Mélodium compatibility.
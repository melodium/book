# Introduction

__Mélodium__ is a language and tool to implement robust data stream, event reaction, distributed software.
It proposes a wide scope of operations available for large kind of data, in very different environments, from higly constrained embedded machines to hyper scalable cloud infrastructure.

Originally designed for scientific research in audio and signal analysis, it has now been extended to handle lot of software problematics.
Working on many platforms and environments, it is capable of handling large amount of data, managing auto-scaling, in real-time with high optimization.

The current book is a summary of its usage and present the purpose of Mélodium, its internal concepts, and how to program and work with it.
Please refer to the [Mélodium Standard Reference](https://doc.melodium.tech/latest/en/) for detailed documentation, and to the [repository](https://gitlab.com/melodium/melodium) for development.
Also take a look to the [Official Project Website](https://melodium.tech/).

> Mélodium and this book are a work in progress, aiming to evolve and grow significantly with time.
> All the informations explained there might no be up-to-date compared to the current state of the project.
> All this work is done with passion and any comment is good to provide.

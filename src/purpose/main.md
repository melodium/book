# Purpose

Mélodium has been created to fill the gap between most of the programming languages and the idea of events and streams.

Most languages, while using many paradigms, see data as individual boxes that need to be moved in memory in a step-by-step approach.
Developer need to define how these boxes are organized and passed as parameter, processed through loops, returned, keeping track of what's happening, and managing error cases in many locations (or assuming all will be fine else program will crash).
This approach can be sufficient in many cases, and lot of languages already did a great job at it.

Now comes the problematic of events and streams.

What if we need a program able to run continously and efficiently on data without being able to assume it will fit in a box, or even in memory? What if we need it to continue to process even if somewhere or sometimes a problem happens? What if we need a program that insure everything to be ok at startup, and take care safely, by design, of errors that may occur?

Here comes Mélodium.
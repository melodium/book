# Software

In this chapter is explained the Mélodium software itself.

Its usage, the command-line interface, the possible project formats, and the supported platforms are all detailed in their own subchapters.


